# Hacknizer Helm-Chart

This repository if the chart for deploying a single Hacknizer Service to
Kubernetes using Helm.


## Running

To deploy, you need to confirm that `Makefile` points to the correct directory
path and follows the right structure. The expected is:

```plain
hacknizer
|___ Chart
|    |___ ...
|    |___ Makefile
|    |___ values.yaml
|___ auth-api
|    |___ ...
|    |___ values.yaml
|___ groups-api
|    |___ ...
|    |___ values.yaml
|___ hackathon-api
|    |___ ...
|    |___ values.yaml
|___ participants-api
|    |___ ...
|    |___ values.yaml
|___ pwa-generator-api
|    |___ ...
|    |___ values.yaml
|___ selection-api
|    |___ ...
|    |___ values.yaml
|___ api-gateway
     |___ ...
     |___ values.yaml

```

To install a single service, enter the `Chart` directory, then run:


```bash
make <target>
```

where `<target>` can be one of the following:
- auth-api
- group-api
- hackathon-api
- participants-api
- pwa-generator-api
- selection-api
- api-gateway

> note: the `api-gateway` expects that all other services are already running
