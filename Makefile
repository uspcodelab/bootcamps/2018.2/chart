REPO=/home/joaofran/workspace/hacknizer

AUTH=auth-api
GROU=groups-api
HACK=hackathon-api
PART=participants-api
PWAG=pwa-generator-api
SELE=selection-api
GATE=api-gateway


####################
##### AUTH-API #####
####################

.PHONY: ${AUTH} del-${AUTH}

## run ##
${AUTH}:
	helm install -n ${AUTH} ${REPO}/Chart \
		--namespace hacknizer \
		--values ${REPO}/${AUTH}/values.yaml

## del ##
del-${AUTH}:
	helm del --purge ${AUTH}



####################
##### GROU-API #####
####################

.PHONY: ${GROU} del-${GROU}

## run ##
${GROU}:
	helm install -n ${GROU} ${REPO}/Chart \
		--namespace hacknizer \
		--values ${REPO}/${GROU}/values.yaml

## del ##
del-${GROU}:
	helm del --purge ${GROU}



####################
##### HACK-API #####
####################

.PHONY: ${HACK} del-${HACK}

## run ##
${HACK}:
	helm install -n ${HACK} ${REPO}/Chart \
		--namespace hacknizer \
		--values ${REPO}/${HACK}/values.yaml

## del ##
del-${HACK}:
	helm del --purge ${HACK}



####################
##### PART-API #####
####################

.PHONY: ${PART} del-${PART}

## run ##
${PART}:
	helm install -n ${PART} ${REPO}/Chart \
		--namespace hacknizer \
		--values ${REPO}/${PART}/values.yaml

## del ##
del-${PART}:
	helm del --purge ${PART}



####################
##### PWAG-API #####
####################

.PHONY: ${PWAG} del-${PWAG}

## run ##
${PWAG}:
	helm install -n ${PWAG} ${REPO}/Chart \
		--namespace hacknizer \
		--values ${REPO}/${PWAG}/values.yaml

## del ##
del-${PWAG}:
	helm del --purge ${PWAG}



####################
##### SELE-API #####
####################

.PHONY: ${SELE} del-${SELE}

## run ##
${SELE}:
	helm install -n ${SELE} ${REPO}/Chart \
		--namespace hacknizer \
		--values ${REPO}/${SELE}/values.yaml

## del ##
del-${SELE}:
	helm del --purge ${SELE}



####################
##### GATE-API #####
####################

.PHONY: ${GATE} del-${GATE}

## run ##
${GATE}:
	helm install -n ${GATE} ${REPO}/Chart \
		--namespace hacknizer \
		--values ${REPO}/${GATE}/values.yaml

## del ##
del-${GATE}:
	helm del --purge ${GATE}



#####################
##### HACKNIZER #####
#####################

.PHONY: hacknizer del-hacknizer

## run ##
hacknizer:
	make ${AUTH}
	make ${GROU}
	make ${HACK}
	make ${PART}
	make ${PWAG}
	make ${SELE}
	make ${GATE}

## del ##
del-hacknizer:
	make del-${GATE}
	make del-${SELE}
	make del-${PWAG}
	make del-${PART}
	make del-${HACK}
	make del-${GROU}
	make del-${AUTH}
